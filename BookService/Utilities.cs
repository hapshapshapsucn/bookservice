﻿using System;

namespace BookService
{
    public class Utilities
    {
        public static int Isbn10to13()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Removes the first and last part of a string, and returns the center part.
        /// </summary>
        /// <param name="firstPart">Part before center</param>
        /// <param name="lastPart">part after center</param>
        /// <returns></returns>
        public static string CenterSubString(string fullString, string firstPart, string lastPart)
        {
            var firstPartLength = firstPart.Length;
            var lastPartLenght = lastPart.Length;
            var centerString = fullString.Substring(firstPartLength,
                fullString.Length - firstPartLength - lastPartLenght);
            return centerString;
        }
    }
}