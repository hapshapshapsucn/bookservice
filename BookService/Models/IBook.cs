﻿using System.Collections.Generic;

namespace BookService.Models
{
    public interface IBook
    {
        long? Isbn10 { get; set; }
        long? Isbn13 { get; set; }
        string Title { get; set; }
        IAuther Auther { get; set; }
        List<IReview> Reviews { get; set; }
    }
}