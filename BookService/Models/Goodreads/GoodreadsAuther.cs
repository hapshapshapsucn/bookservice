﻿namespace BookService.Models.Goodreads
{
    public class GoodreadsAuther : IAuther
    {
        public string Name { get; set; }
    }
}