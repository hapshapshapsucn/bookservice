﻿using System.Collections.Generic;

namespace BookService.Models.Goodreads
{
    public class GoodreadsBook : IBook
    {
        public long? GoodreadsId { get; set; }
        public long? Isbn10 { get; set; }
        public long? Isbn13 { get; set; }
        public string Title { get; set; }
        public IAuther Auther { get; set; }
        public List<IReview> Reviews { get; set; }

        public override string ToString()
        {
            return "Book Info. Title: " + Title + " Auther: " + Auther.Name;
        }
    }
}