﻿namespace BookService.Models.Goodreads
{
    public class GoodreadsReview : IReview
    {
        public string AutherName { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            var returnValue = "Review Info. \nAuther: " + AutherName + ",\nShort description: \n" +
                              Description.Substring(0, 100).Normalize();
            return returnValue;
        }
    }
}