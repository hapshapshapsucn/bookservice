﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BookService.Models.Goodreads
{
    // ReSharper disable All 

    /* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */

    [XmlRoot(ElementName = "Request")]
    public class Request
    {
        [XmlElement(ElementName = "authentication")]
        public string Authentication { get; set; }

        [XmlElement(ElementName = "key")]
        public string Key { get; set; }

        [XmlElement(ElementName = "method")]
        public string Method { get; set; }
    }

    [XmlRoot(ElementName = "books_count")]
    public class Books_count
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "best_book_id")]
    public class Best_book_id
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "reviews_count")]
    public class Reviews_count
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ratings_sum")]
    public class Ratings_sum
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ratings_count")]
    public class Ratings_count
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "text_reviews_count")]
    public class Text_reviews_count
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "original_publication_year")]
    public class Original_publication_year
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "original_publication_month")]
    public class Original_publication_month
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }

        [XmlAttribute(AttributeName = "nil")]
        public string Nil { get; set; }
    }

    [XmlRoot(ElementName = "original_publication_day")]
    public class Original_publication_day
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }

        [XmlAttribute(AttributeName = "nil")]
        public string Nil { get; set; }
    }

    [XmlRoot(ElementName = "original_language_id")]
    public class Original_language_id
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "nil")]
        public string Nil { get; set; }
    }

    [XmlRoot(ElementName = "desc_user_id")]
    public class Desc_user_id
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "default_chaptering_book_id")]
    public class Default_chaptering_book_id
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "default_description_language_code")]
    public class Default_description_language_code
    {
        [XmlAttribute(AttributeName = "nil")]
        public string Nil { get; set; }
    }

    [XmlRoot(ElementName = "author")]
    public class Author
    {
        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "role")]
        public string Role { get; set; }

        [XmlElement(ElementName = "image_url")]
        public Image_url Image_url { get; set; }

        [XmlElement(ElementName = "small_image_url")]
        public Small_image_url Small_image_url { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }

        [XmlElement(ElementName = "average_rating")]
        public string Average_rating { get; set; }

        [XmlElement(ElementName = "ratings_count")]
        public string Ratings_count { get; set; }

        [XmlElement(ElementName = "text_reviews_count")]
        public string Text_reviews_count { get; set; }
    }

    [XmlRoot(ElementName = "best_book")]
    public class Best_book
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "author")]
        public Author Author { get; set; }

        [XmlElement(ElementName = "image_url")]
        public string Image_url { get; set; }

        [XmlElement(ElementName = "small_image_url")]
        public string Small_image_url { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "work")]
    public class Work
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "books_count")]
        public Books_count Books_count { get; set; }

        [XmlElement(ElementName = "ratings_count")]
        public Ratings_count Ratings_count { get; set; }

        [XmlElement(ElementName = "text_reviews_count")]
        public Text_reviews_count Text_reviews_count { get; set; }

        [XmlElement(ElementName = "original_publication_year")]
        public Original_publication_year Original_publication_year { get; set; }

        [XmlElement(ElementName = "original_publication_month")]
        public Original_publication_month Original_publication_month { get; set; }

        [XmlElement(ElementName = "original_publication_day")]
        public Original_publication_day Original_publication_day { get; set; }

        [XmlElement(ElementName = "average_rating")]
        public string Average_rating { get; set; }

        [XmlElement(ElementName = "best_book")]
        public Best_book Best_book { get; set; }

        [XmlElement(ElementName = "reviews_count")]
        public Reviews_count Reviews_count { get; set; }

        [XmlElement(ElementName = "ratings_sum")]
        public Ratings_sum Ratings_sum { get; set; }

        [XmlElement(ElementName = "original_title")]
        public string Original_title { get; set; }

        [XmlElement(ElementName = "original_language_id")]
        public Original_language_id Original_language_id { get; set; }

        [XmlElement(ElementName = "media_type")]
        public string Media_type { get; set; }

        [XmlElement(ElementName = "rating_dist")]
        public string Rating_dist { get; set; }

        [XmlElement(ElementName = "desc_user_id")]
        public Desc_user_id Desc_user_id { get; set; }

        [XmlElement(ElementName = "default_chaptering_book_id")]
        public Default_chaptering_book_id Default_chaptering_book_id { get; set; }

        [XmlElement(ElementName = "default_description_language_code")]
        public Default_description_language_code Default_description_language_code { get; set; }
    }

    [XmlRoot(ElementName = "results")]
    public class Results
    {
        [XmlElement(ElementName = "work")]
        public List<Work> Works { get; set; }
    }

    [XmlRoot(ElementName = "image_url")]
    public class Image_url
    {
        [XmlAttribute(AttributeName = "nophoto")]
        public string Nophoto { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "small_image_url")]
    public class Small_image_url
    {
        [XmlAttribute(AttributeName = "nophoto")]
        public string Nophoto { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "authors")]
    public class Authors
    {
        [XmlElement(ElementName = "author")]
        public List<Author> Author { get; set; }
    }

    [XmlRoot(ElementName = "shelf")]
    public class Shelf
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
    }

    [XmlRoot(ElementName = "popular_shelves")]
    public class Popular_shelves
    {
        [XmlElement(ElementName = "shelf")]
        public List<Shelf> Shelf { get; set; }
    }

    [XmlRoot(ElementName = "book_link")]
    public class Book_link
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }
    }

    [XmlRoot(ElementName = "book_links")]
    public class Book_links
    {
        [XmlElement(ElementName = "book_link")]
        public Book_link Book_link { get; set; }
    }

    [XmlRoot(ElementName = "buy_link")]
    public class Buy_link
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }
    }

    [XmlRoot(ElementName = "buy_links")]
    public class Buy_links
    {
        [XmlElement(ElementName = "buy_link")]
        public List<Buy_link> Buy_link { get; set; }
    }

    [XmlRoot(ElementName = "series")]
    public class Series
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "note")]
        public string Note { get; set; }

        [XmlElement(ElementName = "series_works_count")]
        public string Series_works_count { get; set; }

        [XmlElement(ElementName = "primary_work_count")]
        public string Primary_work_count { get; set; }

        [XmlElement(ElementName = "numbered")]
        public string Numbered { get; set; }
    }

    [XmlRoot(ElementName = "series_work")]
    public class Series_work
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "user_position")]
        public string User_position { get; set; }

        [XmlElement(ElementName = "series")]
        public Series Series { get; set; }
    }

    [XmlRoot(ElementName = "series_works")]
    public class Series_works
    {
        [XmlElement(ElementName = "series_work")]
        public Series_work Series_work { get; set; }
    }

    [XmlRoot(ElementName = "book")]
    public class Book
    {
        [XmlElement(ElementName = "id")]
        public long Id { get; set; }

        [XmlElement(ElementName = "title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "title_without_series")]
        public string Title_without_series { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }

        [XmlElement(ElementName = "small_image_url")]
        public string Small_image_url { get; set; }

        [XmlElement(ElementName = "image_url")]
        public string Image_url { get; set; }

        [XmlElement(ElementName = "num_pages")]
        public string Num_pages { get; set; }

        [XmlElement(ElementName = "work")]
        public Work Work { get; set; }

        [XmlElement(ElementName = "isbn")]
        public string Isbn { get; set; }

        [XmlElement(ElementName = "isbn13")]
        public string Isbn13 { get; set; }

        [XmlElement(ElementName = "average_rating")]
        public string Average_rating { get; set; }

        [XmlElement(ElementName = "ratings_count")]
        public string Ratings_count { get; set; }

        [XmlElement(ElementName = "publication_year")]
        public string Publication_year { get; set; }

        [XmlElement(ElementName = "publication_month")]
        public string Publication_month { get; set; }

        [XmlElement(ElementName = "publication_day")]
        public string Publication_day { get; set; }

        [XmlElement(ElementName = "authors")]
        public Authors Authors { get; set; }
    }

    [XmlRoot(ElementName = "similar_books")]
    public class Similar_books
    {
        [XmlElement(ElementName = "book")]
        public List<Book> Book { get; set; }
    }

    [XmlRoot(ElementName = "search")]
    public class Search
    {
        [XmlElement(ElementName = "query")]
        public string Query { get; set; }

        [XmlElement(ElementName = "results-start")]
        public string Resultsstart { get; set; }

        [XmlElement(ElementName = "results-end")]
        public string Resultsend { get; set; }

        [XmlElement(ElementName = "total-results")]
        public string Totalresults { get; set; }

        [XmlElement(ElementName = "source")]
        public string Source { get; set; }

        [XmlElement(ElementName = "query-time-seconds")]
        public string Querytimeseconds { get; set; }

        [XmlElement(ElementName = "results")]
        public Results Results { get; set; }
    }

    [XmlRoot(ElementName = "GoodreadsResponse")]
    public class GoodreadsResponse
    {
        [XmlElement(ElementName = "Request")]
        public Request Request { get; set; }

        [XmlElement(ElementName = "search")]
        public Search Search { get; set; }

        [XmlElement(ElementName = "book")]
        public Book Book { get; set; }
    }
}