﻿namespace BookService.Models
{
    public interface IReview
    {
        string AutherName { get; set; }
        //TODO: Add date and score?
        string Description { get; set; }
    }
}