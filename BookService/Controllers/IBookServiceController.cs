﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookService.Models;

namespace BookService.Controllers
{
    public interface IBookServiceController
    {
        /// <summary>
        ///     Finds all books matching value, with minimal info.
        /// </summary>
        /// <param name="value">Book title, author or isbn</param>
        /// <returns></returns>
        List<IBook> FindBooks(string value);
        Task<List<IBook>> FindBooksAsync(string value);

        /// <summary>
        /// DETTE ER HVAD DER SKAL VIRKE ANDREAS. SÅ GIVER DET ALT SAMMEN MEST MENING! :D @Andreas @Splint @andreas //TODO MAKE THIS SHIT
        /// </summary>
        /// <param name="isbn"></param>
        /// <returns></returns>
        IBook GetBook(long isbn);

        IBook UpdateBookData(IBook book);
        Task<IBook> UpdateBookDataAsync(IBook book);
        List<IReview> GetBookReviews(IBook book);
        Task<List<IReview>> GetBookReviewsAsync(IBook book);
    }
}