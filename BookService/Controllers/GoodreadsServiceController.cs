﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using BookService.Models;
using BookService.Models.Goodreads;
using HtmlAgilityPack;
using RestSharp;

namespace BookService.Controllers
{
    public class GoodreadsServiceController : IBookServiceController
    {
        private const string BaseUri = "https://www.goodreads.com";
        private readonly string _requestKey;

        public GoodreadsServiceController()
        {
            _requestKey = ConfigurationManager.AppSettings["GoodreadsAPIKey"];
        }

        public List<IBook> FindBooks(string value)
        {
            return FindBooksAsync(value).Result;
        }

        public async Task<List<IBook>> FindBooksAsync(string value)
        {
            var returnValue = new List<IBook>();
            var address = "/search/index.xml";
            var query = "q=" + value;
            var responseAsync = await RequestResponseAsync<GoodreadsResponse>(address, query);
            var response = responseAsync.Search.Results.Works;
            //TODO: Check if works is empty. And if response is null.
            if ((response == null) || (response.Count < 1))
                return null;
            foreach (var work in response)
            {
                var book = new GoodreadsBook();
                long id;
                if (long.TryParse(work.Best_book.Id.ToString(), out id))
                    book.GoodreadsId = id;
                book.Title = work.Best_book.Title;
                book.Auther = new GoodreadsAuther { Name = work.Best_book.Author.Name };
                //TODO: Get ISBN number for book.
                returnValue.Add(book);
            }
            return returnValue;
        }


        public IBook UpdateBookData(IBook book)
        {
            var returnValue = book as GoodreadsBook;
            if (returnValue == null)
                return null; //Should probably throw an applicationException instead.
            var address = "/book/show.xml";
            var query = "text_only=true&id=" + returnValue.GoodreadsId;
            var response = RequestResponse<GoodreadsResponse>(address, query).Book;
            if (response == null)
                return null;
            returnValue.GoodreadsId = response.Id;
            returnValue.Isbn13 = long.Parse(response.Isbn13);
            returnValue.Isbn10 = long.Parse(response.Isbn);
            returnValue.Title = response.Title;
            //returnValue.Auther = new GoodreadsAuther {Name = response.Authors.Author[0].Name}; 
            //Can't get Authers from this response, because RestSharp doesn't support lists with this scheme setup..
            returnValue.Reviews = GetBookReviewsAsync(book).Result.ToList();
            return returnValue;
        }

        public async Task<IBook> UpdateBookDataAsync(IBook book)
        {
            var returnValue = book as GoodreadsBook;
            if (returnValue == null)
                return null; //Should probably throw an applicationException instead.
            var address = "/book/show.xml";
            var query = "text_only=true&id=" + returnValue.GoodreadsId;
            var response = RequestResponse<GoodreadsResponse>(address, query).Book;
            if (response == null)
                return null;
            returnValue.GoodreadsId = response.Id;
            returnValue.Isbn13 = long.Parse(response.Isbn13);
            returnValue.Isbn10 = long.Parse(response.Isbn);
            returnValue.Title = response.Title;
            //returnValue.Auther = new GoodreadsAuther {Name = response.Authors.Author[0].Name}; 
            //Can't get Authers from this response, because RestSharp doesn't support lists with this scheme setup..
            returnValue.Reviews = await GetBookReviewsAsync(book);
            return returnValue;
        }

        public List<IReview> GetBookReviews(IBook book)
        {
            return GetBookReviewsAsync(book).Result;
        }

        public Task<List<IReview>> GetBookReviewsAsync(IBook book)
        {
            if (!book.Isbn13.HasValue)
            {
                book = UpdateBookData(book);
                if (!book.Isbn13.HasValue) throw new ApplicationException(book + " Doesn't have a ISBN13 number.");
            }
            return GetReviewsFromHtmlDom(book.Isbn13.Value);
        }

        private async Task<List<IReview>> GetReviewsFromHtmlDom(long isbn13)
        {
            List<IReview> returnValue;
            var reviewsUrl = BaseUri + "/api/reviews_widget_iframe?isbn=" + isbn13;
            var web = new HtmlWeb();
            var doc = web.Load(reviewsUrl);
            var nodes = doc.DocumentNode.SelectNodes("//div[@id=\"gr_reviews_widget398323\"]/div[@itemprop=\"reviews\"]");

            // tasks;
            //foreach (var node in nodes)
            //{
            var tasks = nodes.Select(n => Task.Run(() => GetReviewFromHtmlDomAsync(n)));
            //}

            //var returnValueAsync = nodes.Select(async n => GetGoodreadsReviewFromHtmlDomAsync(n));
            var taskResults = await Task.WhenAll(tasks);
            returnValue = taskResults.ToList();
            return returnValue;
        }

        private IReview GetReviewFromHtmlDomAsync(HtmlNode node)
        {
            //Console.WriteLine("One started!");
            string auther = null;
            string description = null;

            var nodeChildren = node.ChildNodes;
            var autherNode =
                nodeChildren.FirstOrDefault(
                    n => (n.Name == "span") && n.OuterHtml.Contains("<span class='gr_review_by'>"));
            if (autherNode != null)
                auther = autherNode.FirstChild.NextSibling.InnerText;
            //That'll give the name of whom made the review.

            //Getting description
            var descriptionNode =
                nodeChildren.FirstOrDefault(
                    n => (n.Name == "div") && n.OuterHtml.Contains("<div class=\"gr_review_text\">"));
            if (descriptionNode != null)
            {
                description = descriptionNode.InnerText.Trim();
                //This will only be a very short part of the actual review.

                //Gets the id for the review. //TODO: sourround with try/catch, as short description can be used instead of the long one.
                var reviewIdNode =
                    descriptionNode.ChildNodes.First(
                        n => (n.Name == "a") && n.OuterHtml.Contains("class=\"gr_more_link\""));
                var reviewIdUrl = reviewIdNode.Attributes.First(n => n.Name == "href").Value;
                var reviewIdString = Utilities.CenterSubString(reviewIdUrl, BaseUri + "/review/show/",
                    "?utm_campaign=reviews&amp;utm_medium=widget");
                var reviewId = long.Parse(reviewIdString);

                //Time to get the big review!
                var longDescription = GetBigReviewGoodreadsId(reviewId);
                if (!string.IsNullOrWhiteSpace(longDescription))
                    description = longDescription;

                //Console.WriteLine("returning one");
                //Building the review Object:
                if (!string.IsNullOrWhiteSpace(auther) || !string.IsNullOrWhiteSpace(description))
                //TODO: This should probably cause an exception.
                {
                    var review = new GoodreadsReview
                    {
                        AutherName = auther,
                        Description = description
                    };
                    return review;
                }
            }
            return null;
        }

        public string GetBigReviewGoodreadsId(long reviewId)
        {
            var url = BaseUri + "/review/show/" + reviewId;

            var web = new HtmlWeb();
            var doc = web.Load(url);
            var node = doc.DocumentNode.SelectSingleNode("//div[@itemprop=\"reviewBody\"]");

            return node.InnerText.Trim();
        }

        public IBook GetBookByGoodreadsId(string GoodreadsId)
        {
            throw new NotImplementedException();
        }

        private T RequestResponse<T>(string address, string query) where T : new()
        {
            //var restClient = new RestClient(BaseUri);

            //if (!string.IsNullOrWhiteSpace(query))
            //    query = "&" + query;
            //else
            //    query = "";
            //var requestString = address + "?key=" + _requestKey + query;
            //var request = new RestRequest();
            //request.Resource = requestString;
            ////request.RootElement = "GoodreadsResponse";
            //var stuff = restClient.Execute<T>(request);
            //if (stuff.ErrorException != null)
            //    Console.WriteLine("Request Error? " + stuff.ErrorException);
            ////Console.WriteLine("Error check done");
            //return stuff.Data; //This is made to fail. TODO: Add checks.

            return RequestResponseAsync<T>(address, query).Result;
        }

        private async Task<T> RequestResponseAsync<T>(string address, string query) where T : new()
        {
            var restClient = new RestClient(BaseUri);
            var taskCompletionSource = new TaskCompletionSource<T>();

            if (!string.IsNullOrWhiteSpace(query))
                query = "&" + query;
            else
                query = "";
            var requestString = address + "?key=" + _requestKey + query;
            var request = new RestRequest {Resource = requestString};
            //request.RootElement = "GoodreadsResponse";
            //var stuff = restClient.Execute<T>(request);
            restClient.ExecuteAsync<T>(request, (response) =>
            {
                if (response.ErrorException != null)
                {
                    throw new ApplicationException("Error retrieving Goodreads data.", response.ErrorException);
                }
                taskCompletionSource.SetResult(response.Data);
            });
            return await taskCompletionSource.Task; //This is made to fail. TODO: Add checks.
        }
    }
}