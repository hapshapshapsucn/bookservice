﻿using System;
using System.Configuration;
using BookService.Controllers;

namespace BookService
{
    public class BookServiceFactory
    {
        public static IBookServiceController GetBookServiceController()
        {
            IBookServiceController returnValue;
            var provider = ConfigurationManager.AppSettings["BookServiceProvider"];
            switch (provider)
            {
                case "Goodreads":
                    returnValue = new GoodreadsServiceController();
                    break;
                default:
                    throw new ApplicationException(provider + " Isn't implemented.");
            }
            return returnValue;
        }
    }
}