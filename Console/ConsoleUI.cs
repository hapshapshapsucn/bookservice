﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookService;
using BookService.Controllers;
using BookService.Models;

namespace Console
{
    class ConsoleUi
    {
        private static bool _exit;
        static void Main(string[] args)
        {

            System.Console.WriteLine("Welcome to a tiny BookService application.");
            while (!_exit)
            {
                PrintMainMenu();

            }
            //string input = System.Console.ReadLine();
            //FindBooks(input);
            //System.Console.WriteLine("all books from query printed.");
            //System.Console.ReadLine();
        }

        private static void PrintMainMenu()
        {
            string message = "This is your options: \n " +
                             "Press '0' to exit.\n" +
                             "Press '1' to find a book.";
            System.Console.WriteLine("\n" + message);
            System.Console.Write("Option: ");
            var option = System.Console.ReadLine();
            switch (option)
            {
                case "0":
                    _exit = true;
                    break;
                case "1":
                    FindBooks();
                    break;
                default:
                    System.Console.WriteLine(option + " Is not a valid option. Try Again.");
                    break;
            }
        }

        private static void FindBooks()
        {
            IBookServiceController goodreadsService = BookServiceFactory.GetBookServiceController();
            System.Console.Write("Search: ");
            var books = goodreadsService.FindBooks(System.Console.ReadLine());
            System.Console.WriteLine("The following books have been found: ");
            if (books == null || books.Count == 0)
            {
                System.Console.Write("No books matchin the search parameters where found.");
                return;
            }
            for (int i = 0; i < books.Count; i++)
            {
                System.Console.WriteLine("Book number: " + i + " " + books[i]);
            }
            System.Console.WriteLine("Write the number for the book you want to get reviews for: ");
            var bookChoice = System.Console.ReadLine();
            int bookInt;
            if (!int.TryParse(bookChoice, NumberStyles.Integer, new NumberFormatInfo(), out bookInt))
            {
                System.Console.Write("That's not a valid number.");
                return;
            }
            var reviews = goodreadsService.GetBookReviews(books[bookInt]);
            System.Console.WriteLine("The Following reviews have been found: ");
            System.Console.WriteLine();
            for (int i = 0; i < reviews.Count; i++)
            {
                System.Console.WriteLine("Review number: " + i + " " + reviews[i] + "\n");
            }

            System.Console.Write("Write the number for the review you want to read more from: ");
            var reviewChoice = System.Console.ReadLine();
            int reviewInt;
            if (!int.TryParse(reviewChoice, NumberStyles.Integer, new NumberFormatInfo(), out reviewInt))
            {
                System.Console.Write("That's not a valid number.");
                return;
            }
            System.Console.WriteLine("Book review: \n" + reviews[reviewInt].Description);
        }

        private static void FindBooks(string input)
        {
            IBookServiceController goodreadsService = BookServiceFactory.GetBookServiceController();
            var books = goodreadsService.FindBooks(input);
            books.ForEach(System.Console.WriteLine);
        }

        private static string GetBookInfo(IBook book)
        {
            string value = "Book Info. Title: " + book.Title + " Auther: " + book.Auther.Name;
            return value;
        }
    }
}
