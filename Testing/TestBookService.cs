﻿using System;
using BookService;
using BookService.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass]
    public class TestBookService
    {
        [TestMethod]
        public void TestControllerCreation()
        {
            IBookServiceController controller = BookServiceFactory.GetBookServiceController();
            Assert.IsNotNull(controller);
        }
    }
}
