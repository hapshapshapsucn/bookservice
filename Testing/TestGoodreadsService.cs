﻿using System;
using System.Linq;
using BookService.Controllers;
using BookService.Models;
using BookService.Models.Goodreads;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass]
    public class TestGoodreadsService
    {
        private const string AutherName = "Per Petterson";
        private const string BookTitle = "Out Stealing Horses";
        private const long Isbn13 = 9781555974701;

        private const string ReviewSubPart =
            "The novel opens with a tragedy that befalls the narrator\'s best friend in that long-ago summer, and it seems that will become the fulcrum of the plot -- but it turns out only to be a tangential part of the story. Unbeknownst to the boy, this event will deeply affect his relationship with his father, and it is only as time goes on that he will learn his father\'s full history and how his friend\'s crisis fits into that larger picture, which includes the revelation that his father had been an operative for the Norwegian resistance during World War II.";

        private IBook FindBook(string value)
        {
            IBookServiceController goodreadsService = new GoodreadsServiceController();
            var books = goodreadsService.FindBooks(value);
            Assert.IsNotNull(books);
            return books.FirstOrDefault(n => n.Title == BookTitle && n.Auther.Name == AutherName);
        }

        [TestMethod]
        public void TestFindBooksAuther()
        {
            IBook book = FindBook(AutherName);
            Assert.IsNotNull(book);
            Assert.AreEqual(BookTitle, book.Title);
            Assert.AreEqual(AutherName, book.Auther.Name);
        }

        [TestMethod]
        public void TestFindBooksTitle()
        {
            IBook book = FindBook(BookTitle);
            Assert.IsNotNull(book);
            Assert.AreEqual(BookTitle, book.Title);
            Assert.AreEqual(AutherName, book.Auther.Name);
        }

        [TestMethod]
        public void TestFindBooksIsbn()
        {
            IBook book = FindBook(Isbn13.ToString());
            Assert.IsNotNull(book);
            Assert.AreEqual(BookTitle, book.Title);
            Assert.AreEqual(AutherName, book.Auther.Name);
        }

        [TestMethod]
        public void TestFindBookGoodreadsId()
        {
            IBook book = FindBook(BookTitle);
            GoodreadsBook gbook = book as GoodreadsBook;
            Assert.IsNotNull(gbook);
            Assert.IsNotNull(gbook.GoodreadsId);
        }

        [TestMethod]
        public void TestUpdateBookData() //TODO: Remove this test. It's pointless.
        {
            IBook book = FindBook(BookTitle);
            IBook bookUpdated = new GoodreadsServiceController().UpdateBookData(book);
            Assert.IsNotNull(bookUpdated);
            Assert.AreEqual(BookTitle, bookUpdated.Title);
            Assert.AreEqual(AutherName, bookUpdated.Auther.Name);
            Assert.IsNotNull(bookUpdated.Reviews);
            Assert.IsTrue(bookUpdated.Reviews.Count > 1);
        }

        [TestMethod]
        public void TestGetBookReview()
        {
            GoodreadsServiceController controller = new GoodreadsServiceController();
            IBook book = FindBook(Isbn13.ToString());
            IReview review = controller.GetBookReviews(book)[0];
            Assert.IsNotNull(review);
            Assert.IsNotNull(review.Description);//TODO: Add more.
            Assert.IsTrue(review.Description.Contains(ReviewSubPart));
        }
    }
}
